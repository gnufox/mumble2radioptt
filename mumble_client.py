# # 
# Here be description text


import pymumble_py3
from pymumble_py3.callbacks import PYMUMBLE_CLBK_SOUNDRECEIVED
import time
import threading
import serial
import sys, getopt


nick = "PTT-CLIENT"
port = 64738  # port number

serial_device = None
ptt_state = 0
talk_timer = 0
talk_timer_lock = threading.Lock()

allowed_users = None
showhash = False

def check_user(user):
    """ check if the user is alloed to key the PTT of the radio """
    try:
        return allowed_users[user.get_property('name')] == user.get_property('hash')
    except:
        return False
    #endtry
#enddef

def sound_received_handler(user, soundchunk):
    """ push PTT if sound comes from correct user name(s) (not from radio) """
    global talk_timer, ptt_state, serial_device

    if showhash:
        print(f"{user.get_property('name')} = {user.get_property('hash')}")
    #endif

    if check_user(user):
        with talk_timer_lock:
            talk_timer = 2
            if ptt_state == 0:
                print("Pushing PTT")
                serial_device.setRTS(1)
                ptt_state = 1
            #endif
        #endwith
    #endif
#enddef


def print_help():
    """
    mumble_client.py -s <SERVER> -p <PASSWD> -P <PTT_DEVICE> -U <username:hash,username:hash> --showhash
    
    -s SERVER        Name of the mumble server to connect to
    -p PASSWD        Mumble Server password  
    -P PTT_DEVICE    Path to the serial device used for PTT 
    -U username:hash Multiple combinations of allowed username and hash, separated by ':'
                     example: User1:Hash1,User2:Hash2,...
    --showhash       Print username and hash of the user that is currently talking
    """
    print(print_help.__doc__)


def main(argv):

    global talk_timer, ptt_state, allowed_users, serial_device, showhash
    server = None
    password = None
    ptt_device = None

    try:
        opts, args = getopt.getopt(argv, "s:p:P:U:",["showhash"])
    except getopt.GetoptError:
        print_help()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()
        elif opt in "-s":
            server = arg
        elif opt in "-p":
            password = arg
        elif opt in "-P":
            ptt_device = arg
        elif opt in "-U":
            # Add allowed users to dictionary
            try:
                allowed_users = dict()
                userlist = arg.split(',')
                for u in userlist:
                    username = u.split(':')[0]
                    userhash = u.split(':')[1]

                    allowed_users[username] = userhash
                #endfor
            except:
                print("ERROR: wrong input for -U") #TODO: better error message
            #endtry
        elif opt in "--showhash":
            showhash = True
        #endif
    #endfor

    if not server or not password or not ptt_device or not allowed_users:
        print("\n\n\nERROR: arguments missing\n\n")
        print_help()
        sys.exit()
    #endif

    # Set up serial
    serial_device = serial.Serial(ptt_device)
    serial_device.setRTS(0)

    # Spin up a client and connect to mumble server
    mumble = pymumble_py3.Mumble(server, nick, password=password, port=port)
    # set up callback
    mumble.callbacks.set_callback(PYMUMBLE_CLBK_SOUNDRECEIVED, sound_received_handler)
    mumble.set_receive_sound(1)  # Enable receiving sound from mumble server
    mumble.start()
    mumble.is_ready()  # Wait for client is ready


    # Main "Idle"-Loop (counting down PTT timer)
    while True:
        time.sleep(0.5)
        with talk_timer_lock:
            if talk_timer == 0:
                if ptt_state == 1:
                    print("Releasing PTT")
                    serial_device.setRTS(0)
                    ptt_state = 0
            else:
                print("Doing nothing in main")
                talk_timer = talk_timer - 1
            #endif
        #endif
    #endwhile


if __name__ == "__main__":
    main(sys.argv[1:])